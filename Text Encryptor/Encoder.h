//
//  Encoder.h
//  ShifrLozung
//
//  Created by Denis on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CWLSynthesizeSingleton.h"

typedef enum {encoderErrorNoError = 0, encoderErrorLozung = 1, encoderErrorText = 2} encoderError;

@interface Encoder : NSObject
{
    NSMutableArray *alphabet;
}
@property (nonatomic, readonly) int alphabetCount;
CWL_DECLARE_SINGLETON_FOR_CLASS(Encoder)
-(encoderError)checkTheLozung:(NSString*)loz andTheTextField:(NSString*)text;

-(NSString*)codeText:(NSString*)text withLozung:(NSString*)lozung;
-(NSString*)decodeText:(NSString*)text withLozung:(NSString*)lozung;
-(NSString*)generateLozungWithLenght:(int)lenght;
+(NSString*)clearLozungOfDoubles:(NSString*)lozung;
@end
