//
//  ViewController.m
//  ShifrLozung
//
//  Created by Denis on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController
@synthesize encoder = _encoder;
@synthesize lozungTextField, textView, shifrBut, slidingLozungGenerator, hiddenSwitch, howtoButton, aboutButton,programControl,aboutView,howtoView,howtoBut,howtoReturnBut,donateButton, aboutReturnBut;

#pragma mark -

-(IBAction)backgroundTouched:(id)sender
{
    if (textView.isFirstResponder)
    {
        [textView resignFirstResponder];
    }
    else if (lozungTextField.isFirstResponder)
    {
        [lozungTextField resignFirstResponder];
    }
}

-(IBAction)checkLozungAndText
{
    encoderError checkError = [self.encoder checkTheLozung:lozungTextField.text andTheTextField:textView.text];
    NSLog(@"check: %i", (int)checkError);
    if (checkError != encoderErrorNoError)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Entry error" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        switch (checkError) {
            case encoderErrorLozung:
                alert.message = @"Incorrect keyword!";
                break;
                
            case encoderErrorText:
                alert.message = @"Incorrect text!";
                break;
                
            default:
                alert.message = @"unknown error";
                break;
        }
        [alert show];
        [alert release];
    }
    else
    {
        // No error so we can encode
        // 0 - зашифровать, 1 - расшифровать
        if (shifrBut.selectedSegmentIndex == 0)
        {
            NSLog(@"No error in text fields so we will do Encode");
            NSString *encodedText = [self.encoder codeText:textView.text withLozung:lozungTextField.text];
            if (encodedText != nil)
            {
                NSLog(@"Log encoded text: %@", encodedText);
                textView.text = [NSString stringWithFormat:@"%@", encodedText];
            }
        }
        else if (shifrBut.selectedSegmentIndex == 1)
        {
            NSLog(@"No error in text fields so we will do Decode");
            NSString *encodedText = [self.encoder decodeText:textView.text withLozung:lozungTextField.text];
            if (encodedText != nil)
            {
                NSLog(@"Log encoded text: %@", encodedText);
                textView.text = [NSString stringWithFormat:@"%@", encodedText];
            }
        }
    }
}

-(IBAction)generateLozung
{
    lozungTextField.text = [NSString stringWithFormat:@"%@", [self.encoder generateLozungWithLenght:slidingLozungGenerator.value]];
}

-(IBAction)secureHideLozung
{
    if (lozungTextField.isFirstResponder)
    {
        [lozungTextField resignFirstResponder];
        lozungTextField.secureTextEntry = hiddenSwitch.on;
        [lozungTextField becomeFirstResponder];
    }
    else
    {
        lozungTextField.secureTextEntry = hiddenSwitch.on;
    }
    
}

-(IBAction)aboutButDown
{
    [self.view addSubview:aboutView];
    if (textView.isFirstResponder)
    {
        [textView resignFirstResponder];
    }
    else if (lozungTextField.isFirstResponder)
    {
        [lozungTextField resignFirstResponder];
    }
}

-(IBAction)aboutReturnButDown
{
    [aboutView removeFromSuperview];
}

-(IBAction)howtoButDown
{
    [self.view addSubview:howtoView];
    if (textView.isFirstResponder)
    {
        [textView resignFirstResponder];
    }
    else if (lozungTextField.isFirstResponder)
    {
        [lozungTextField resignFirstResponder];
    }
}

-(IBAction)donateButDown
{
    NSString* launchUrl = @"https://sites.google.com/site/donatemyproject/home";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: launchUrl]];
}

-(IBAction)howtoReturnButDown
{
    [howtoView removeFromSuperview];
}

#pragma mark -

-(void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    _encoder = [Encoder sharedEncoder];
    slidingLozungGenerator.maximumValue = TE_GENERATOR_MAX_VALUE;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

@end
