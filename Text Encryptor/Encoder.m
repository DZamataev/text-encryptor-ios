//
//  Encoder.m
//  ShifrLozung
//
//  Created by Denis on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Encoder.h"

@implementation Encoder

CWL_SYNTHESIZE_SINGLETON_FOR_CLASS(Encoder)

-(id)init
{
    self = [super init];
    if (self)
    {
        alphabet = [NSMutableArray new];
        for(unichar a = 0x0020; a <= 0x007F; a++)
        {
            [alphabet addObject:[NSString stringWithCharacters:&a length:1]];
        }
//        for(char a = 'a'; a <= 'z'; a++)
//        {
//            [alphabet addObject:[NSString stringWithFormat:@"%c", a]];
//        }
//        for(char a = '0'; a <= '9'; a++)
//        {
//            [alphabet addObject:[NSString stringWithFormat:@"%c", a]];
//        }
//        for(unichar a = 0x0410; a <= 0x044F; a++)
//        {
//            [alphabet addObject:[NSString stringWithCharacters:&a length:1]];
//        }
//
//        [alphabet addObject:[NSString stringWithFormat:@" "]];
//        [alphabet addObject:[NSString stringWithFormat:@"."]];
//        [alphabet addObject:[NSString stringWithFormat:@","]];
//        [alphabet addObject:[NSString stringWithFormat:@"-"]];
//        [alphabet addObject:[NSString stringWithFormat:@"!"]];
//        [alphabet addObject:[NSString stringWithFormat:@"?"]];
        
//        NSLog(@"--------------------");
//        NSString *alphabetString = [NSString new];
//        NSLog(@"Alphabet is set:");
//        for (NSString *str in alphabet)
//        {
//            NSLog(@"  %@",str);
//            alphabetString = [NSString stringWithFormat:@"%@%@",alphabetString,str];
//        }
//        NSLog(@"one string:%@",alphabetString);
//        NSLog(@"--------------------");
    }
    return self;
}


-(int)alphabetCount
{
    return [alphabet count];
}

-(encoderError)checkTheLozung:(NSString *)loz andTheTextField:(NSString *)text
{
    if ((loz==nil) || (loz.length == 0))
        return encoderErrorLozung;
    
    if ((text==nil) || (text.length == 0))
        return encoderErrorText;

    // lozung
    for (int i = 0; i<loz.length; i++)
    {
        unichar ch = [loz characterAtIndex:i];
        NSString *checkStr = [NSString stringWithCharacters:&ch length:1];
        bool ok = NO;
        for (NSString *str in alphabet)
        {
            if ([checkStr isEqualToString:str])
            {
                ok = YES;
                break;
            }
        }
        if (!ok)
        {
            NSLog(@"Lozung contains invalid letter: %@ (unichar:%c)",checkStr,ch);
            return encoderErrorLozung;
        }
    }
    
    // text
    for (int i = 0; i<text.length; i++)
    {
        unichar ch = [text characterAtIndex:i];
        NSString *checkStr = [NSString stringWithCharacters:&ch length:1];
        bool ok = NO;
        for (NSString *str in alphabet)
        {
            if ([checkStr isEqualToString:str])
            {
                ok = YES;
                break;
            }
        }
        if (!ok)
        {
            NSLog(@"Text contains invalid letter: %@ (unichar:%c)",checkStr,ch);
            return encoderErrorText;
        }
    }
    
    
    return encoderErrorNoError;
}

-(NSString*)codeText:(NSString*)text withLozung:(NSString*)lozung
{
    // очистим лозунг
    lozung = [NSString stringWithFormat:@"%@",[Encoder clearLozungOfDoubles:lozung]];
    
    // скопируем исходный алфавит
    NSMutableArray *oldAlphabet = [[NSMutableArray new] autorelease];
    for (NSString *str in alphabet)
    {
        [oldAlphabet addObject:[NSString stringWithFormat:@"%@",str]];
    }
    
    // создаем новый алфавит
    NSMutableArray *newAlphabet = [[NSMutableArray new] autorelease];
    // заносим в него все символы из лозунга, попутно удаляя их из исходного алфавита
    for (int i = 0; i < lozung.length; i++)
    {
        unichar cha = [lozung characterAtIndex:i];
        [newAlphabet addObject:[NSString stringWithCharacters:&cha length:1]];
        NSString *checkStr = [NSString stringWithFormat:@"%c",cha];
        int remIndex = -1;
        for (NSString *str in oldAlphabet)
        {
            if ([str isEqualToString:checkStr])
                remIndex = [oldAlphabet indexOfObject:str];
        }
        if (remIndex != -1)
            [oldAlphabet removeObjectAtIndex:remIndex];
    }
    // теперь добавим "очищенный" исходный алфавит к новому
    [newAlphabet addObjectsFromArray:oldAlphabet];
    NSLog(@"New alphabet:");
    for (NSString *str in newAlphabet)
        NSLog(@"  %@",str);
    // сопоставим количество символов в каждом из алфавитов
    NSLog(@"Count of new alphabet:%i, count of eternal alphabet: %i", [newAlphabet count], [alphabet count]);
    // оно должно быть одинаковым
    
    // теперь представим текст в новом алфавите
    NSString *newText = [NSString new];
    for (int i = 0; i<text.length; i++)
    {
        unichar ch = [text characterAtIndex:i];
        NSString *chStr = [NSString stringWithCharacters:&ch length:1];
        int indInOldAlphabet = -1;
        for (NSString *str in alphabet)
        {
            if ([str isEqualToString:chStr])
            {
                indInOldAlphabet = [alphabet indexOfObject:str];
                break;
            }
        }
        if (indInOldAlphabet != -1)
        {
            // мы нашли индекс в старом алфавите
            NSString *newLetter = [NSString stringWithFormat:@"%@",[newAlphabet objectAtIndex:indInOldAlphabet]];
            newText = [NSString stringWithFormat:@"%@%@",newText,newLetter];
        }
    }
    
    return newText;
}

-(NSString*) decodeText:(NSString *)text withLozung:(NSString *)lozung
{
    // очистим лозунг
    lozung = [NSString stringWithFormat:@"%@",[Encoder clearLozungOfDoubles:lozung]];
    
    // скопируем исходный алфавит
    NSMutableArray *oldAlphabet = [[NSMutableArray new] autorelease];
    for (NSString *str in alphabet)
    {
        [oldAlphabet addObject:[NSString stringWithFormat:@"%@",str]];
    }
    
    // создаем новый алфавит
    NSMutableArray *newAlphabet = [[NSMutableArray new] autorelease];
    // заносим в него все символы из лозунга, попутно удаляя их из исходного алфавита
    for (int i = 0; i < lozung.length; i++)
    {
        unichar cha = [lozung characterAtIndex:i];
        
        [newAlphabet addObject:[NSString stringWithCharacters:&cha length:1]];
        NSString *checkStr = [NSString stringWithFormat:@"%c",cha];
        int remIndex = -1;
        for (NSString *str in oldAlphabet)
        {
            if ([str isEqualToString:checkStr])
                remIndex = [oldAlphabet indexOfObject:str];
        }
        if (remIndex != -1)
            [oldAlphabet removeObjectAtIndex:remIndex];
    }
    // теперь добавим "очищенный" исходный алфавит к новому
    [newAlphabet addObjectsFromArray:oldAlphabet];
    NSLog(@"New alphabet:");
    NSLog(@"%@", newAlphabet);
    // сопоставим количество символов в каждом из алфавитов
    NSLog(@"Count of new alphabet:%i, count of eternal alphabet: %i", [newAlphabet count], [alphabet count]);
    // оно должно быть одинаковым
    
    
    // теперь представим текст в исходном алфавите
    NSString *newText = [NSString new];
    for (int i = 0; i<text.length; i++)
    {
        unichar ch = [text characterAtIndex:i];
        NSString *chStr = [NSString stringWithCharacters:&ch length:1];
        int indInNewAlphabet = -1;
        for (NSString *str in newAlphabet)
        {
            if ([str isEqualToString:chStr])
            {
                indInNewAlphabet = [newAlphabet indexOfObject:str];
                break;
            }
        }
        if (indInNewAlphabet != -1)
        {
            // мы нашли индекс в новом алфавите
            NSString *newLetter = [NSString stringWithFormat:@"%@",[alphabet objectAtIndex:indInNewAlphabet]];
            newText = [NSString stringWithFormat:@"%@%@",newText,newLetter];
        }
    }
    
    return newText;

}

-(NSString*)generateLozungWithLenght:(int)lenght
{
    NSString *newText = [NSString new];
    NSMutableArray *newAlphabet = [NSMutableArray arrayWithArray:alphabet];
    for (int i = 0; i< lenght; i++)
    {
        int randIndex = rand()%[newAlphabet count];
        NSString *newLetter = [NSString stringWithFormat:@"%@", [newAlphabet objectAtIndex:randIndex]];
        newText = [NSString stringWithFormat:@"%@%@",newText,newLetter];
        [newAlphabet removeObjectAtIndex:randIndex];
    }
    return newText;
}

+(NSString*)clearLozungOfDoubles:(NSString *)lozung
{
    // работаем с лозунгом, чистим его от повторяющихся букв
    // lozung
    NSMutableArray *lozungArray = [NSMutableArray new];
    for (int i = 0; i<lozung.length; i++)
    {
        unichar ch = [lozung characterAtIndex:i];
        NSString *chStr = [NSString stringWithCharacters:&ch length:1];
        [lozungArray addObject:chStr];
    }
    NSString *lozungNew = [NSString new];
    NSMutableArray *lettersToDelete = [NSMutableArray new];
    for (NSString *letter in lozungArray)
    {
        bool firstEntry = YES;
        for (NSString *checkLetter in lozungArray)
        {
            if ([checkLetter isEqualToString:letter])
            {
                if (firstEntry)
                {
                    firstEntry = NO;
                }
                else
                {
                    [lettersToDelete addObject:checkLetter];
                }
            }
        }
    }
    
    for (NSString *let in lettersToDelete)
    {
        [lozungArray removeObjectIdenticalTo:let];
    }
    [lettersToDelete release];
    
    for (NSString *letter in lozungArray)
    {
        lozungNew = [NSString stringWithFormat:@"%@%@", lozungNew, letter];
    }
    NSLog(@"Cleared lozung: '%@'",lozungNew);
    [lozungArray release];
    // лозунг очищен от повторяющихся букв
    return lozungNew;
}


@end





//    // создаем алфавиты для лозунга
//    NSMutableArray *alphabets = [NSMutableArray new];
//    for (int i = 0; i < lozung.length; i++)
//    {
//        unichar curChar = [lozung characterAtIndex:i];
//        NSString *letter = [NSString stringWithFormat:@"%c",curChar];
//        int letterIndexInAlphabet = -1;
//        for (NSString *str in alphabet)
//        {
//            if ([letter isEqualToString:str])
//            {
//                letterIndexInAlphabet = [alphabet indexOfObject:str];
//                break;
//            }
//        }
//        NSMutableArray *alp = [NSMutableArray new];
//        // в начало ставим символ из лозунга
//        [alp addObject:[NSString stringWithFormat:@"%c", curChar]];
//        // а потом все символы из алфавита идущие после него
//        
//    }