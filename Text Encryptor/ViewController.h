//
//  ViewController.h
//  ShifrLozung
//
//  Created by Denis on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Encoder.h"

#define TE_GENERATOR_MAX_VALUE 16;

@interface ViewController : UIViewController
{
    Encoder *_encoder;
    UIView *aboutView;
    UIView *howtoView;
    UIControl *programControl;
    UITextField *lozungTextField;
    UITextView *textView;
    UISegmentedControl *shifrBut;
    UISlider *slidingLozungGenerator;
    UISwitch *hiddenSwitch;
    
    UIButton *donateBut;
    UIButton *aboutReturnBut;
    UIButton *howtoBut;
    UIButton *howtoReturnBut;
}
@property (nonatomic, assign) Encoder *encoder;
@property (nonatomic, retain) IBOutlet UIView *aboutView;
@property (nonatomic, retain) IBOutlet UIControl *programControl;
@property (nonatomic, retain) IBOutlet UITextField *lozungTextField;
@property (nonatomic, retain) IBOutlet UITextView *textView;
@property (nonatomic, retain) IBOutlet UISegmentedControl *shifrBut;
@property (nonatomic, retain) IBOutlet UISlider *slidingLozungGenerator;
@property (nonatomic, retain) IBOutlet UISwitch *hiddenSwitch;
@property (nonatomic, retain) IBOutlet UIButton *aboutButton;
@property (nonatomic, retain) IBOutlet UIButton *howtoButton;
@property (nonatomic, retain) IBOutlet UIButton *donateButton;
@property (nonatomic, retain) IBOutlet UIButton *aboutReturnBut;
@property (nonatomic, retain) IBOutlet UIView *howtoView;
@property (nonatomic, retain) IBOutlet UIButton *howtoBut;
@property (nonatomic, retain) IBOutlet UIButton *howtoReturnBut;

-(IBAction)checkLozungAndText;
-(IBAction)generateLozung;
-(IBAction)secureHideLozung;
- (IBAction)backgroundTouched:(id)sender;
-(IBAction)aboutButDown;
-(IBAction)howtoButDown;
-(IBAction)donateButDown;
-(IBAction)aboutReturnButDown;
-(IBAction)howtoReturnButDown;
@end
